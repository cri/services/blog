# Blog

## Quick start

```
git submodule update --init --recursive
# Either
hugo server --disableFastRender
# or
docker run --rm -p 1313:1313 -v $(pwd):/src jojomi/hugo:0.74.3 hugo server --bind "0.0.0.0" --disableFastRender
```

## Deployment

The blog is deployed automatically via the gitlab-ci to blog.cri.epita.fr (via
our S3 API). Master is deployed to blog.cri.epita.fr whereas each branch
deploys to blog.dev.cri.epita.fr
