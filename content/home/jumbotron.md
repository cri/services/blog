+++
widget = "blank"
headless = true

title = "Blog du CRI"
weight = 1

[design]
  columns = '1'

[design.background]
  color = "#23252f"
  text_color_light = true

[design.spacing]
  padding = ["30px", "0", "10px", "0"]
+++

Le CRI (centre des ressources informatiques) était un service de l'EPITA,
anciennement chargé de l'opération de l'infrastructure et du matériel affectés
aux activités pédagogiques. Ce service a depuis fusionné avec le Lab SI de
l'EPITA, chargé de développer les outils pédagogiques de l'école. Cette fusion a
donné naissance à la « Forge » qui dispose désormais de ces deux fonctions. Ce
blog reste en ligne à des fins d'archives.
