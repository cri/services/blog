reset

set terminal pngcairo

set title 'Running builds over time'

set timefmt '%s'
set xdata time

set format x '%d/%m/%y %H:%M'

set xtics rotate by -90

plot "running-builds-over-time" using 1:2 with lines title 'running builds'
