---
title: Elasticsearch, ODFE et ouverture de la sécurité
date: 2019-05-24 20:20:00 +0200
authors:
  - zarak
toc: true
---

# Un peu de contexte

Elastic est une entreprise proposant plusieurs produits open-source dont Elasticsearch,
le plus connu, et le coeur de la suite Elastic (anciennement stack ELK pour Elasticsearch Logstash Kibana).
La suite Elastic est un ensemble de produits permettant la collecte, la transformation,
le stockage et la visualisation de logs et de metrics. Les usages sont multiples :
- Monitoring d'infrastructure
- Interface centralisée de visualisation des logs
- Moteur de recherche et d'indexation
- ...

Le modèle économique d'Elastic est relativement classique : un coeur open-source
auquel s'ajoute des killer-features payantes, avec license. La license n'est
pas "automatique", dans le sens ou il faut forcément passer par leur
service commercial pour avoir un devis du prix de la license en fonction
des besoins et de qui la demande.

# Des licenses et des retournements de situation

Avant une refonte du système, Elasticsearch et compagnie étaient complètement open-sources
avec une license assez laxe, et un plugin avec des sources fermées et une
license restrictive, x-pack, se greffait par dessus la stack ELK. Par la suite,
x-pack a été intégré directement à Elasticsearch, le modèle de license [a changé](https://discuss.elastic.co/t/elastic-stack-and-gdpr-reloaded/121121),
et les ennuis pour Elastic sont arrivés : [ODFE](https://opendistro.github.io/for-elasticsearch/).

![logo opendistro](https://opendistro.github.io/for-elasticsearch/assets/media/logos/OpenDistro-logo.svg)

Du fait de la politique commerciale très aggressive d'Elastic (les prix
étant très élevés, le système de license par devis assez contraignant, ...), et
suite aux changements de license d'Elasticsearch, Amazon a décidé d'intervenir.
Amazon, via AWS/EC2, fourni énormement de serveurs Elasticsearch. Ils ont
donc décider d'utiliser le coeur open-source d'Elasticsearch, et d'y greffer
un gros plugin maison et d'appeler ça ODFE pour Open Distro For Elasticsearch.
Ce plugin ajoute une partie des fonctionnalités les plus demandée, payantes
chez Elastic, et les fournies gratuitement. En plus, Amazon propose une LTS de 5ans,
et un code open-source. Le rêve de tous les frustrés des licenses Elastic. Il est également
intéressant de noter qu'Elastic ne fourni pas de license à but éducatif pour les
universités (malheureusement pour nous).

La réaction d'Elastic a été rapide : [Un billet de blog a été posté quelques jours seulement après l'annonce d'Amazon](https://www.elastic.co/fr/blog/on-open-distros-open-source-and-building-a-company)

# La nouveauté du 20 mai

Cependant, Elastic a décidé visiblement de répondre par l'escalade avec Amazon,
puisqu'ils ont annoncé le 20 mai 2019 qu'ils décidaient de rendre totalement
gratuit à tous l'accès aux fonctionnalités de sécurité de la suite Elastic.
C'est une attaque assez nette à Amazon, la sécurité étant le fer de lance d'ODFE
contre le modèle de license d'Elastic.

# Du coup, que choisir ?

Ainsi, la question se pose : vaut-il mieux disposer de la version gratuite
d'Elasticsearch qui embarque des fonctionnalités supplémentaire par rapport
à la version totalement open-source, et désormais la sécurité, ou vaut-il
mieux considérer ODFE ?

Le tableau ci dessous résume sommairement les différences entre les points
qu'implémentent l'un des deux partis. *ODFE* désigne Open Distro For Elasticsearch,
et *Elasticsearch* désigne ici la version d'Elasticsearch qui inclus la version
gratuite d'Elastic suite (ancienement x-pack).

| Fonctionalité                  | ODFE        | Elasticsearch    |
| -------------                  | ----        | -------------    |
| HTTPS                          | présent     | présent          |
| Role-based control access      | présent     | présent          |
| Authc LDAP/PKI/AD              | présente    | toujours absente |
| Authc statique                 | présente    | présente         |
| Audit logging                  | présente    | absente          |
| Alerting                       | présent     | absent           |
| Cartes et Canvas               | absent      | présent          |
| Index Life Management          | absent      | présent          |
| Interface de gestion des rôles | équivalent  | équivalent       |
| Self-monitoring                | perfectible | excellent        |
| Machine-Learning               | absent      | très très léger  |
| Logs UI                        | absente     | présente         |
| Infrastructure UI              | absente     | présente         |
| Uptime UI                      | absente     | présente         |
| SQL                            | présent     | présent          |

Sources:
- [elastic.co/fr/subscriptions](https://www.elastic.co/fr/subscriptions)
- [opendistro.github.io/for-elasticsearch-docs/](https://opendistro.github.io/for-elasticsearch-docs/)
- Notre tests de comparaison

Si l'on s'en réfère à ce tableau (qui contient peut-être quelques inexactitudes),
il est difficile de déterminer quelle solution choisir. Il est vrai que l'authentification
par LDAP, Active Directory, etc, est un argument de poids envers ODFE. Cependant,
ce choix à un coût, notamment sur l'interface utilisateur. Kibana est en effet
démuni de beaucoup de ses fonctionalités avec ODFE, notamment le logs visualiser
qui est au CRI une fonctionnalité qu'on apprécie particulièrement.

**Attention !** Il est important de noter que ODFE se base sur la version
open-source d'elasticsearch et rajoute son plugin. Par conséquent, il y un décalage
de sortie des versions entre ODFE et Elasticsearch, ce dernier ayant un rythme de
sortie assez soutenu.

Lien vers les repos github:<br/>
https://github.com/elastic/elasticsearch<br/>
https://github.com/opendistro-for-elasticsearch
