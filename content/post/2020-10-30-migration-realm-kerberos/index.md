---
title: Migration du realm Kerberos utilisé par le CRI
date: 2020-10-30 20:30:00 +0200
authors:
  - mareo
---

Hier soir nous avons procédé à une opération de maintenance sur l'AFS qui a
abouti à un changement de *realm* Kerberos et de nom de cellule AFS. Cette
opération, qui a pris quelques heures, est en réalité l'aboutissement d'un
projet débuté il y a plus d'un an avec le début de la réécriture de l'intranet
CRI et ça méritait selon nous un article sur ce blog. Nous y présentrons
rapidement le principe de Kerberos, la façon dont il a été utilisé initialement
au CRI, les problèmes que ça a posé ainsi que les solutions apportées. Nous
terminerons enfin sur les choses que nous souhaitons mettre en place pour la
suite.


Introduction
------------

Kerberos est un protocole de d'authentification développé par le MIT dans le
cadre du projet [Athena](https://en.wikipedia.org/wiki/Project_Athena). Ce
projet avait pour objet de produire un environnement de travail informatique
distribué à l'échelle d'un campus universitaire et qui a eu une influence
considérable sur l'informatique d'entreprise : outre Kerberos, on lui doit
également X, le système graphique utilisé sous linux et il a influencé le
developpement de
[LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) et
d'[Active Directory](https://en.wikipedia.org/wiki/Active_Directory).

Kerberos tient son nom de Cerbère (*Κέρβερος* en grec), le gardien des enfer
dans la mythologie grecque. Le choix de savoir si « gardien » ou « infernal »
est l'adjectif le plus appoprié pour le décrire est laissé à l'appréciation de
chacun.

Le problème auquel il répond est « comment authentifier des communications de
façon sécurisée sur un réseau auquel on ne fait pas confiance ». Le but est de
pouvoir garantir à un service que l'utilisateur qui communique avec lui est
bien la personne qu'il prétend être tout comme assurer à l'utilisateur qu'il ne
communique pas avec quelqu'un qui se fait passer pour un service. Tout Kerberos
est basé sur de la cryptographie symétrique et un tiers de confiance : le KDC
(*key distribution center*).

Au sein d'un « *realm* » Kerberos, tous les acteurs placent leur confiance dans
le KDC qui dispose pour chacun des « *principals* » (un *principal* Kerberos
peut se référer à un utilisateur comme à un service) d'une clé secrète dont il
connait la valeur. Pour les utilisateurs, cette clé secrète est dérivée d'un
hash de son mot de passe (on ne peut donc pas retrouver le mot de passe à
partir de la clé secrète).

Le rôle du KDC va, à partir de la connaissance des clés de chacun des
*principals* et par un usage assez astucieux de la cryptographie symétrique,
être en mesure de fournir à chaque utilisateur des « tickets » pour des
services. Ces tickets contiennent une clé de session, qui permet éventuellement
de chiffrer ou signer les communications, ainsi qu'une valeur uniquement
déchiffrable par le service et qui contient le *principal* de l'utilisateur qui
l'a obtenu.

Par exemple si vous vous connectez sur l'intranet CRI avec Kerberos, vous
obtiendrez un ticket pour le service qui correspond à l'intranet, pour l'AFS
vous obtiendrez un autre ticket pour celui de l'AFS, etc. Afin d'éviter que
vous ayez à taper votre mot de passe à chaque fois que vous voulez obtenir un
ticket pour un service, Kerberos prévoit un type de ticket particulier : le TGT
(*ticket granting ticket*), qui permet, pendant une durée limitée (12h à
EPITA), d'obtenir des tickets sans avoir à indiquer son mot de passe. C'est
grâce à ce mécanisme que Kerberos permet de faire du SSO (*single sign-on*, ou
*authentification unique* en français).

Kerberos a initialement été mis en place au CRI par Nicolas Géniteau, qui en a
été le premier responsable, afin de pouvoir sécuriser les homes des étudiants :
NFS avait été envisagé pour y accéder avant que Nicolas ne se décide à utiliser
AFS, plus complexe à mettre en œuvre mais également beaucoup plus fiable et
performant. Ces deux protocoles ne peuvent efficacement faire de contrôle
d'accès qu'avec Kerberos.

Les services du CRI qui demandent de l'authentification par mot de passe
peuvent les vérifier via LDAP ou Kerberos. En pratique, afin d'éviter d'avoir à
dupliquer les informations d'authentification, LDAP utilise Kerberos pour
vérifier les mots de passe au travers d'un mécanisme appellé
[SASL](https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer).

Le realm « historique »
-----------------------

Le *realm* choisi initialement par Nicolas était alors `CRI.EPITA.NET` (par
convention les noms des *realms* Kerberos s'écrivent en majuscule) et collait
avec le nom de domaine utilisé par le CRI l'époque : cri.epita.net. Ce nom de
domaine est en train d'être progressivement abandonné au profit de
cri.epita.fr et ne subsiste encore que dans les hostnames des postes des salles
machines et dans certains réseaux internes du CRI.

Ce *realm* souffrait de deux problèmes :
  1. le premier est assez anectotique, son nom ne correspond pas au nom de
     domaine vers lequel le CRI héberge ses services, il aurait fallu qu'il se
     nomme « `CRI.EPITA.FR` ».
  2. le second est plus gênant, ses *principals* ne correspondent pas
     exactement aux logins des utilisateurs. En effet, le *principal* Kerberos
     de l'utilisateur `marin.hannache` dans ce *realm* est
     `marin_hannache@CRI.EPITA.NET`

Le choix d'avoir des *principals* Kerberos qui ne correspondent pas aux login
est lié à une limitation technique de l'AFS (elle-même est la conséquence de
choix fait dans la mise en œuvre de la version 4 de Kerberos dans les années
80) : les noms des utilisateurs dans l'AFS ne peuvent pas contenir le caractère
« point » (« . »).

Cette limitation avait peu de conséquences pour les logins suivant l'ancien
format (par exemple `hannac_m`) mais, depuis 2017, le Bocal a décidé
d'abandonner ces identifiants et de ne plus se baser que sur les adresses
mails.  Par conséquent, les logins qui en découlent contiennent des points.

Pour surmonter cette difficulté Nicolas avait fait le choix, pour les
*principals* Kerberos, de remplacer les points dans les logins par des
underscores, la connexion aux machines de l'école étant basée sur Kerberos, un
script PAM vient faire la conversion et utilise le mot de passe de
l'utilisateur pour obtenir un TGT. Pour la petite histoire, le même script
exécute ensuite la commande `aklog` qui permet d'obtenir un ticket pour l'AFS.

Le *principal* ainsi transformé est consigné dans l'entrée LDAP de
l'utilisateur, ce qui permet au serveur LDAP de reconnaître les logins «
classique » et d'arriver à authentifier les utilisateurs avec Kerberos
lorsqu'on lui demande de le faire, en dépit de la disparité entre le login et
le *principal*.

Cependant Kerberos peut être utilisé pour d'autres choses que l'AFS et le CRI
souhaite depuis longtemps en étendre les usages pour permettre de faire du «
vrai SSO » : être connecté sur une machine de l'école permettrait d'avoir accès
à l'intranet CRI sans avoir à entrer de mot de passe ou d'accéder à ses dépôts
Git sans clé SSH. En plus de rendre l'utilisation des machines de l'école un
peu plus agréable et de retirer une épine dans le pied des SUP qui ont déjà
assez de chose à apprendre avec Git pour ne pas avoir à se soucier du
fonctionnement de SSH, ça aurait également l'avantage de simplifier
l'environnement d'examen machine.

En effet, il est aujourd'hui demandé aux utilisateurs de retaper leur mot de
passe dans le script de mise en place des examens après s'être connecté à leur
session. Cette étape sert en réalité à générer une clé SSH et à la mettre en
ligne sur l'intranet CRI au début de l'examen et elle ne serait plus nécessaire
si l'authentification auprès du serveur Git se faisait via Kerberos.

La volonté d'étendre l'utilisation de Kerberos se heurte néanmoins au problème
posé par le fait que les *principals* ne correspondent pas aux logins puisque
certains services ne sont pas prévus pour prendre en charge une telle
disparité.


Le nouvel intranet
------------------

En avril 2019, quand le développement du nouvel intranet CRI a débuté, nous
nous sommes posé la question de la place de Kerberos dans notre infrastructure.
Ce n'est pas pour rien si l'une des premières *merge request* à être validée
était [une implémentation de
SPNEGO](https://gitlab.cri.epita.fr/cri/services/intranet/-/merge_requests/4),
le protocole qui permet d'utiliser Kerberos pour s'authentifier auprès d'un
site internet. Et nous avons commencé à regarder quelles solutions pouvaient
être apportées au problème de la disparité des *principals*.

Nous nous sommes alors rendu compte que l'AFS pouvait gérer des *principals*
avec des points, pour peu qu'on prenne certaines dispositions et qu'une option
soit activée dans la configuration des serveurs. Nous avons alors commencé à
regarder comment il pourrait être possible de renommer les principals.

En pratique cette opération est possible, pour peu qu'on ne change pas le
*realm*, mais elle implique l'invalidation des mots de passe car le principal
est utilisé comme salt lors de la dérivation de la clé secrète. Ces deux
limitations nous posaient problème car d'une part on ne voulait pas garder un
*realm* Kerberos qui ne corresponde pas au nom de domaine que nous utilisons et
d'autre part nous pensons que l'invalidation de la totalité des mots de passe
aurait engendré beaucoup de difficultés et d'incompréhensions pour les
utilisateurs, quels que soient les efforts que nous aurions consacrés à
communiquer à ce sujet.

Au final, nous avons décidé de concevoir le nouvel intranet de façon à ce que
plusieurs principals Kerberos puissent être rattachés à un utilisateur,
l'intranet garde pour chacun d'entre eux un flag qui indique si le mot de passe
qu'il contient est valide. Si ce n'est pas le cas, il est mis à jour lors de la
prochaine connexion réussie à l'intranet. Ainsi, si le mot de passe d'un
principal Kerberos n'est pas valide, il peut être mis à jour de façon
transparente pour l'utilisateur, pour peu qu'il se connecte sur l'intranet avec
son mot de passe.

Pendant la période de transition, il est possible de se connecter avec SPNEGO
sur l'intranet avec n'importe quel *principal* Kerberos rattaché à un
utilisateur, quel que soit son *realm*.


Mise en place du nouveau realm
------------------------------

La création du nouveau *realm* Kerberos `CRI.EPITA.FR` a eu lieu dans la nuit
du 19 octobre 2020, les nouveaux *principals* ont été créés à cette occasion
avec des clés aléatoire, et assez progressivement, leurs mot de passe ont été
changés aux fur et à mesure que les utilisateurs se connectaient à l'intranet.

Néanmoins ça n'allait pas aussi vite qu'espéré car de nombreux utilisateurs
utilisent la fonctionnalité de connexion avec Office365, cette connexion se
faisant sans mot de passe, il n'était pas possible de synchroniser les nouveaux
*principals*. À partir du 24, [une modification de
l'intranet](https://gitlab.cri.epita.fr/cri/services/intranet/-/commit/80e400c35543595d8c31a7a18d35bd79b962b205)
vient interdire la connexion par ce biais quand un des *principals* de
l'utilisateur n'est pas synchronisé. D'où le message d'erreur que certains ont
pu voir :

> Social auth failed: one of your Kerberos principal is out of sync, you must
> sign-in with your login and password.

Le graphique ci-dessous montre la progression de la synchronisation des
nouveaux *principals* Kerberos. Il y a assez peu de points avant le 24 octobre
car c'est à partir de cette date que j'ai commencé à en suivre l'évolution de
manière plus précise (d'où la ligne toute droite au début).

![](plot.png)

Il était initialement prévu d'attendre encore quelques jours que la majorité
des utilisateurs actifs aient eu l'occasion de mettre à jour leur nouveau
*principal* avant de basculer les postes des salles machine et l'AFS sur le
nouveau *realm*, mais l'annonce du confinement nous a convaincu d'avancer nos
plans car l'authentification Kerberos va nous permettre de vous donner accès à
votre AFS depuis chez vous. Cette migration a donc eu lieu de façon anticipée
hier soir. Ça signifie que depuis minuit, seules les personnes s'étant
connectées avec leur identifiants CRI sur notre intranet après le 19 octobre
sont en mesure de se connecter aux postes des salles machines.

Le nombre de *principals* synchronisés à ce jour (2531), correspond bien à nos
estimations d'utilisateurs actifs (et l'applanissement de la courbe corrobore
cette analyse). On va néanmoins identifier et prévenir les 52 étudiants du
cycle préparatoire dont le nouveau *principal* Kerberos n'est pas à jour car
ils vont passer des examens sur machine début novembre et auront besoin de
pouvoir se connecter sur leur session.

Les images des salles machines ont été regénérées de façon à ce que la
connexion se fasse via le nouveau *realm* Kerberos, la cellule AFS a également
changé de nom : `/afs/cri.epita.fr` remplace l'ancien chemin
`/afs/cri.epita.net`. Si vous aviez des scripts qui se basent sur l'ancien
chemin il faudra les mettre à jour.

Les images de VM ont également été mises à jour pour vous permettre de
bénéficier des changements de configuration qui ont été faits pour que vous
puissiez accéder à votre AFS ou aux dépôt Git du CRI avec Kerberos. Attention,
les dépôts Git assistants ne sont pas accessible avec Kerberos à ce jour, les
ING1 devront donc continuer à configurer une clé SSH pour pouvoir y accéder.


Configuration de son OS pour utiliser Kerberos
----------------------------------------------

***Il n'est pas utile de suivre ces instructions si vous utilisez la VM du
CRI.***

Vous pouvez installer les utilitaires Kerberos depuis chez vous assez
simplement, la configuration a été faite de notre côté de façon à ce que le KDC
soit accessible depuis l'extérieur de l'école. Pour les OS basés sur debian il
faudra installer le paquet `krb5-user`, sur Archlinux il faudra installer le
paquet `krb5`.

Vous pouvez ajouter les deux entrées ci-dessous dans votre fichier de
configuration SSH (`~/.ssh/config`) pour activer l'authentification SSH avec
Kerberos (via le protocole GSSAPI) :

```
Host git.cri.epita.fr
	GSSAPIAuthentication yes

Host ssh.cri.epita.fr
	GSSAPIAuthentication yes
	GSSAPIDelegateCredentials yes
```

Si vous voulez bénéficier du SSO sur l'intranet CRI sous firefox, vous devez
vous rendre dans l'éditeur de configuration (`about:config`) et ajouter
`cri.epita.fr` à l'option `network.negotiate-auth.trusted-uris`.

Une option similaire existe pour Chrome, mais il faut la passer en ligne de
commande :

```shell
chromium --auth-server-whitelist="cri.epita.fr"
```


Utilisation de Kerberos
-----------------------

Vous devez tout d'abord utiliser `kinit` pour obtenir un TGT. Le flag `-f` est
important si vous voulez pouvoir accéder à votre AFS au travers d'une connexion
SSH (il est mis implicitement dans la VM CRI).

```shell
$ kinit -f marin.hannache@CRI.EPITA.FR
Password for marin.hannache@CRI.EPITA.FR:
```

### Cloner ses dépôts Git

```shell
$ git clone marin.hannache@git.cri.epita.fr:perso/marin.hannache/test.git
Cloning into 'test'...
```


### Récupérer tout le contenu de son AFS

```shell
$ sftp -r marin.hannache@ssh.cri.epita.fr:/afs/cri.epita.fr/user/m/ma/marin.hannache/u afs
```

**Attention :** cette commande peut prendre du temps s'il y a beaucoup de
données dans votre AFS.

**Attention (bis) :** le chemin d'accès de votre AFS dépend de votre login, par
exemple pour `xavier.login` ce serait
`/afs/cri.epita.fr/user/x/xa/xavier.login/u`


### Explorer son AFS de façon interactive

```shell
$ sftp marin.hannache@ssh.cri.epita.fr
Connected to ssh.cri.epita.fr.
sftp> cd /afs/cri.epita.fr/user/m/ma/marin.hannache/u/
sftp> ls -la
drwxr-xr-x    3 marin.hannache 8000         2048 Sep 14 05:55 .
drwxrwxrwx    3 marin.hannache 8000         2048 Sep 14  2017 ..
drwxr-xr-x    9 marin.hannache 8000         2048 Oct 29 23:55 .confs
```

**Il ne sera pas possible d'obtenir un vrai shell sur la machine et par
conséquent les commandes `scp` et `rsync` ne pourront pas fonctionner pour
récupérer vos données.**

Réferez-vous à la manpage de `sftp(1)` pour avoir plus de détails sur la façon
dont cette commande peut être utilisée.

### Monter son AFS dans la VM

Vous pouvez utiliser la commande `sshfs` pour monter votre AFS à l'intérieur de
votre VM, c'est la façon la plus simple d'accéder à vos fichiers :

```shell
$ mkdir -p afs
$ sshfs -o reconnect marin.hannache@ssh.cri.epita.fr:/afs/cri.epita.fr/user/m/ma/marin.hannache/u/ afs
```

**Attenion :** Les fichiers ne seront plus accessibles à l'expiration de votre
ticket et vous aurez alors une erreur `permission denied`. Vous pouvez refaire
`kinit -f` pour obtenir un nouveau TGT, puis
`umount afs/ && sshfs -o reconnect marin.hannache@ssh.cri.epita.fr:/afs/cri.epita.fr/user/m/ma/marin.hannache/u/ afs`
pour rétablir la connexion.

Conclusion
----------

Nous sommes en train d'étudier la possibilité de vous donner accès nativement à
votre AFS depuis chez vous, sans passer par une connexion SSH, et d'arriver à
faire fonctionner le formulaire de connexion dans votre VM, mais ça pose des
problèmes de confidentialité que nous devons surmonter avant que ça ne puisse
être envisageable.

Dans l'intervalle, le serveur SSH `ssh.cri.epita.fr` sera utilisé pour vous
servir de point d'entrée dans le réseau de l'école, et il permettra, par le
biais d'une fonctionnalité appellée « ProxyJump », de vous connecter à distance
sur les postes des salles machines. Nous allons cependant prendre encore le
temps d'effectuer certains réglages avant que cette possibilité ne vous soit
offerte car elle impliquera qu'une même machine soit potentiellement utilisée
par plusieurs utilisateurs, et nous voulons être certain que ça ne pose pose
pas de difficultés nouvelles.

Dans quelques jours nous décomissionerons définitivement l'ancien serveur
Kerberos et supprimerons les *principals* du *realm* `CRI.EPITA.NET` de
l'intranet CRI, non sans une certaine émotion car c'est le dernier service qui
date des débuts du CRI et c'est une petite page de l'histoire de l'école qui se
tourne.
