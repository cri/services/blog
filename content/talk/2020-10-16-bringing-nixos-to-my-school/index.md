---
title: "Bringing NixOS to my school"
event: NixCon 2020
event_url: https://2020.nixcon.org/
location: Online
address:
  street:
  city:
  region:
  postcode:
  country:
summary: The challenges of bringing NixOS to the PIE.
abstract: |
  EPITA, a French school of engineers in computer intelligence, is bringing
  NixOS to its students. Here is a presentation of the challenges of
  maintaining an infrastructure of more than 800 machines used by students.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-10-16T18:30:00+02:00
date_end: 2020-10-16T18:55:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: 2020-09-13T15:57:52+02:00

authors:
  - risson
# tags:
#   - NixOS
#   - PIE

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: https://cfp.nixcon.org/nixcon2020/talk/DHGPLN/

url_code:
url_pdf:
url_video: https://youtu.be/7sQa04olUA0?t=24057

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
