---
title: Formation OpenStack
event: Formations CRI
event_url: https://web.microsoftstream.com/channel/4658cdca-260b-46cd-b161-5a0447ac4a24
location: Online
address:
  street:
  city:
  region:
  postcode:
  country:
summary: Formation à l'utilisation d'OpenStack
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-03-30T14:00:00+02:00
date_end: 2021-03-30T17:00:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: 2020-09-13T15:57:52+02:00

authors:
  - risson
# tags:
#   - OpenStack
#   - Formation

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

links:
  - name: Partie 1 - Horizon
    url: https://web.microsoftstream.com/video/8739c05d-dcf9-455c-a6dd-9d9ffcb74140?channelId=4658cdca-260b-46cd-b161-5a0447ac4a24
  - name: Partie 2 - Terraform
    url: https://web.microsoftstream.com/video/ab7f9a4b-6a6c-415b-85bc-4f4eca931381?channelId=4658cdca-260b-46cd-b161-5a0447ac4a24

# Optional filename of your slides within your talk's folder or a URL.
# url_slides:

url_code: https://gitlab.cri.epita.fr/cri/documentation/openstack
# url_pdf:
# url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
