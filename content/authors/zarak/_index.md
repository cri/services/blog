---
# Display name
title: 'Cyril `zarak` Duval'

# Username (this should match the folder name)
authors:
- zarak

# Is this the primary user of the site?
superuser: false

# Role/position
role: Étudiant

# Short bio (displayed in user profile at end of posts)
bio: Étudiant à EPITA de la promo 2020. Membre du CRI de janvier 2018 à janvier 2020. Root CRI + Assistant (YAKA / ACU) à EPITA.

interests:
- Ansible
- Docker
- Elasticsearch
- HTTPS
- Linux
- Nginx

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: cv
  icon_pack: ai
  link: 'https://cyrilduval.fr/'
- icon: envelope
  icon_pack: fas
  link: 'mailto:cyril1.duval@epita.fr'
- icon: address-book
  icon_pack: fas
  link: 'https://www.linkedin.com/in/cyril-duval/'

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- 2020
---

Des projets sur lesquels j'ai pu travailler:
- Ce blog
- Divers projets pour ma scolarité (projets `ing1` : malloc, find, bittorrent, ...)
- ISDE, application récoltant des millions de logs par jour avec analyse comportementale
- MOTF, setup d'une stack complète de Monitoring avec benchmarking + analyse sécurité (Telegraf, Elasticsearch, openLDAP, nginx, ...)
- Setup d'une stack ELK modifiée, via ansible et gitlab-ci pour le CRI
- Spider Web Server, projet de 7 semaines destiné aux troisième années à EPITA (Server HTTP/S, reverse proxy, load balancing, timeouts, ...)
- [Projet de recrutement LSE](https://www.lse.epita.fr/data/sujets/subject_my-dbg.pdf) (pour rejoindre le CRI): un debugger en C avec ptrace(2)
- Un micro-kernel ([K](https://k.lse.epita.fr/))
