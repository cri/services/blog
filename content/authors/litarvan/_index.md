---
# Display name
title: Adrien 'litarvan' Navratil

# Username (this should match the folder name)
authors:
- litarvan

# Is this the primary user of the site?
superuser: false

# Role/position
role: Étudiant

# Short bio (displayed in user profile at end of posts)
bio: Étudiant à EPITA de la promo 2024

interests: []

education: []

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- 2024
---

My work can be found on [Github](https://github.com/Litarvan)
