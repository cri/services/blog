---
# Display name
title: Jean-Baptiste Bussignies

# Username
authors:
- jb

superuser: false

role: Étudiant

bio: Étudiant à l'EPITA de la promotion 2024

interests: []

education: []

social:

user_groups:
- 2024
---
